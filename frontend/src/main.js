import Vue from 'vue'
import App from './App.vue'
import router from "./router/index" //z index.js
import axiosInstance from "@/code/http";
import TokenManager from "./code/token-manager";
import Authorization from "./code/authorization";

Vue.config.productionTip = false

Vue.prototype.$http = axiosInstance;

export const tokenManager = new TokenManager();
tokenManager.renew();
Vue.prototype.$tokenManager = tokenManager;

export const authorization = new Authorization();
Vue.prototype.$authorization = authorization;

new Vue({
  router: router,
  render: h => h(App),
}).$mount('#app')

