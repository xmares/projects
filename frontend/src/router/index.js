import Vue from "vue"
import VueRouter from "vue-router";
import {tokenManager} from "../main";
import Welcome from "./views/Welcome-view";
import Login from "./views/Profiles/Login-view";
import Register from "./views/Profiles/Register-view";
import Home from "./views/Home-view.vue";
import NotFound from "./views/NotFound-view";
import Profile from "./views/Profiles/Profile-view";
import AllProfiles from "./views/Profiles/AllProfiles-view";
import CreatePet from "./views/Pets/CreatePet-view";
import AllPets from "./views/Pets/AllPets-view";
import GetPet from "@/router/views/Pets/Pet-view";
import CreateTreatment from "./views/Treatments/CreateTreatment-view";
import GetTreatment from "./views/Treatments/Treatment-view";
import AllTreatments from "./views/Treatments/AllTreatments-view";
import CreateDrug from "./views/Drugs/CreateDrug-view";
import GetDrug from "./views/Drugs/Drug-view";
import AllDrugs from "./views/Drugs/AllDrugs-view";
import Notify from "./views/Notify-view";
import CreateWeekShiftsView from "./views/Shifts/CreateWeekShifts-view";
import AllWeeks from "./views/Shifts/AllWeeks-view";
import GetWeek from "./views/Shifts/Week-view";
import GetDay from "./views/Shifts/Day-view";
import CreateShift from "./views/Shifts/CreateShift-view";
import GetShift from "./views/Shifts/Shift-view";
import GetReservationSlotsByShiftId from "./views/Reservations/AllReservationSlotsByShift-view";
import GetReservationSlotsByDayId from "./views/Reservations/AllReservationSlotsByDay-view";
import CreateReservation from "./views/Reservations/CreateReservation-view";

Vue.use(VueRouter)

const ROUTES_NAMES = {
    home: "home",
    welcome: "welcome",
    login: "login",
    register: "register",
    notFound: "notFound",
    getUser: "getUser",
    getProfiles: "getProfiles",
    profile: "profile",
    createPet: "createPet",
    allPets: "allPets",
    getPet: "getPet",
    createTreatment: "createTreatment",
    getTreatment: "getTreatment",
    allTreatments: "allTreatments",
    createDrug: "createDrug",
    getDrug: "getDrug",
    allDrugs: "allDrugs",
    notify: "notify",
    CreateWeekShiftsView: "createWeekShifts",
    AllWeeks: "allWeeks",
    GetWeek: "getWeek",
    GetDay: "getDay",
    CreateShift: "createShift",
    GetShift: "getShift",
    GetReservationSlotsByShiftId : "getReservationSlotsByShiftId",
    GetReservationSlotsByDayId : "getReservationSlotsByDayId",
    CreateReservation: "createReservation",


}

const routes = [
    {path: "/login", component: Login, name: ROUTES_NAMES.login},
    {path: "/register", component: Register, name: ROUTES_NAMES.register},
    {path: "", component: Welcome, name: ROUTES_NAMES.welcome},
    {path: "/home", component: Home, name: ROUTES_NAMES.home, meta: {requireAuth: true}},
    {path: "/users", component: AllProfiles, name: ROUTES_NAMES.getProfiles, meta: {requireAuth: true}},
    {path: "/user/:id", component: Profile, name: ROUTES_NAMES.profile,  meta: {requireAuth: true}},
    {path: "/pet", component: CreatePet, name: ROUTES_NAMES.createPet,  meta: {requireAuth: true}},
    {path: "/pet/:id", component: GetPet, name: ROUTES_NAMES.getPet,  meta: {requireAuth: true}},
    {path: "/pets", component: AllPets, name: ROUTES_NAMES.allPets,  meta: {requireAuth: true}},
    {path: "/treatment", component: CreateTreatment, name: ROUTES_NAMES.createTreatment,  meta: {requireAuth: true}},
    {path: "/treatment/:id", component: GetTreatment, name: ROUTES_NAMES.getTreatment,  meta: {requireAuth: true}},
    {path: "/treatments", component: AllTreatments, name: ROUTES_NAMES.allTreatments, meta: {requireAuth: true}},
    {path: "/drug", component: CreateDrug, name: ROUTES_NAMES.createDrug, meta: {requireAuth: true}},
    {path: "/drug/:id", component: GetDrug, name: ROUTES_NAMES.getDrug, meta: {requireAuth: true}},
    {path: "/drugs", component: AllDrugs, name: ROUTES_NAMES.allDrugs, meta: {requireAuth: true}},
    {path: "/notify", component: Notify, name: ROUTES_NAMES.notify},
    {path: "/generateNextWeek", component: CreateWeekShiftsView, name: ROUTES_NAMES.CreateWeekShiftsView, meta: {requireAuth: true}},
    {path: "/weeks", component: AllWeeks, name: ROUTES_NAMES.AllWeeks, meta: {requireAuth: true}},
    {path: "/week/:id", component: GetWeek, name: ROUTES_NAMES.GetWeek, meta: {requireAuth: true}},
    {path: "/day/:id", component: GetDay, name: ROUTES_NAMES.GetDay, meta: {requireAuth: true}},
    {path: "/shift", component: CreateShift, name: ROUTES_NAMES.CreateShift, meta: {requireAuth: true}},
    {path: "/shift/:id", component: GetShift, name: ROUTES_NAMES.GetShift, meta: {requireAuth: true}},
    {path: "/reservationSlotsByShift/:id", component: GetReservationSlotsByShiftId, name: ROUTES_NAMES.GetReservationSlotsByShiftId, meta: {requireAuth: true}},
    {path: "/reservationSlotsByDay/:id", component: GetReservationSlotsByDayId, name: ROUTES_NAMES.GetReservationSlotsByDayId, meta: {requireAuth: true}},
    {path: "/reservation", component: CreateReservation, name: ROUTES_NAMES.CreateReservation, meta: {requireAuth: true}},



    {path: "*", component: NotFound, name: ROUTES_NAMES.notFound},
];

const router = new VueRouter({routes, mode: "history"}); //oddělání # z url - history mode -  možný problém při nasazení

router.beforeEach((to, from, next) => {
    if (to.meta != null && to.meta.requireAuth === true){
        //kontrola tokenu
        if(tokenManager.isUserLogged()){
            next();
        } else{
            next({name:"login"});
        }
    } else {
        next();
    }
})

export default router;