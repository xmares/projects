import {LS_TOKEN_KEY} from "./constants";
import axiosInstance from "./http";

export default class tokenManager{
    token = null;
    setToken (token){
        this.token = token;
        localStorage.setItem(LS_TOKEN_KEY, token);
        axiosInstance.defaults.headers["Authorization"] = `Bearer ${token}`; //token do default hlavicek - posila se s kazdym requestem
    }

    logout(){
        this.token = null;
        localStorage.removeItem(LS_TOKEN_KEY);
        delete axiosInstance.defaults.headers["Authorization"];
    }

    renew(){
        const token = localStorage.getItem(LS_TOKEN_KEY);
        if (token != null){
            this.setToken(token);
        }
    }

    getPayload(){
        if(this.token != null){
            const parts = this.token.split(".");
            const rawData = atob(parts[1]); //dekoduje token na string
            return JSON.parse(rawData);
        }
        return null;
    }

    isUserLogged(){
        return this.token != null;
    }
}
