export default class Authorization {


    authorizationLevel10(role){
        return role === "Customers" || role === "Assistants" || role === "Doctors" || role === "Administrators";
    }
    authorizationLevel20(role){
        return role === "Assistants" || role === "Doctors" || role === "Administrators";
    }
    authorizationLevel30(role){
        return role === "Doctors" || role === "Administrators";
    }
    authorizationLevel40(role){
        return role === "Administrators";
    }

    getLevelOfAuthorization (role){
        if (this.authorizationLevel40(role))
            return 40;
        else if (this.authorizationLevel30(role))
            return 30;
        else if (this.authorizationLevel20(role))
            return 20;
        else if (this.authorizationLevel10(role))
            return 10;
        return 0;
    }

    /*
    async unauthorizedRedirectHome(){
        await this.$router.push({name: 'home'});
    }

     */

}