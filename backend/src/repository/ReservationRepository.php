<?php

namespace App\repository;

use DI\Container;
use PDO;

class ReservationRepository
{
    private PDO $db;

    public function __construct(Container $container)
    {
        $this->db = $container->get('db');
    }

    public function getReservationSlotsByShiftId(int $shiftId): array|bool
    {
        $stmt = $this->db->prepare("SELECT * FROM ReservationSlots WHERE ShiftId = :shiftId");
        $stmt->bindValue("shiftId", $shiftId);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function deleteReservationSlotById(int $id): bool
    {
        $stmt = $this->db->prepare("DELETE FROM ReservationSlots WHERE ReservationSlotId = :id");
        $stmt->bindValue(":id", $id);
        return $stmt->execute();
    }

    public function getReservationSlotsByDayId(int $id): array|bool
    {
        $stmt = $this->db->prepare("SELECT ReservationSlotId, Beginning, Free, ReservationSlots.ShiftId, ReservationId FROM Shifts 
                JOIN ReservationSlots on Shifts.ShiftId = ReservationSlots.ShiftId
                WHERE DayId = :id");
        $stmt->bindValue(":id", $id);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getProcedures(): array|bool
    {
        $stmt = $this->db->prepare("SELECT * FROM Procedures");
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function createReservation(mixed $data)
    {
        try {
            $this->db->beginTransaction();
            $stmt = $this->db->prepare("INSERT INTO Reservations (ProcedureId, PetId) VALUES (:procedureId, :petId)");
            $stmt->bindValue("procedureId", $data["ProcedureId"]);
            $stmt->bindValue("petId", $data["PetId"]);
            $stmt->execute();
            $this->pairReservationWithSlot($this->db->lastInsertId(), $data["ReservationSlotId"]);
            $this->db->commit();
        } catch (\PDOException $e){
            $this->db->rollBack();
            throw $e;
        }

    }

    private function pairReservationWithSlot(int $reservationId, mixed $ReservationSlotId)
    {
        $stmt = $this->db->prepare("UPDATE ReservationSlots SET ReservationId = :reservationId, Free = false WHERE ReservationSlotId = :id");
        $stmt->bindValue("id", $ReservationSlotId);
        $stmt->bindValue("reservationId", $reservationId);
        $stmt->execute();
    }

}