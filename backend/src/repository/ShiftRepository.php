<?php

namespace App\repository;

use DI\Container;
use PDO;
use function DI\add;

class ShiftRepository
{
    private PDO $db;

    public function __construct(Container $container)
    {
        $this->db = $container->get('db');
    }

    /**
     * Procedura vygeneruje sedm po sobě jdoucích dní, které patří do daného týdne dle parametru. Dny jsou uloženy do databáze dní.
     * @param array $week jakému týdnu patří vygenerované dny
     * @return void
     */

    public function generateDaysOfWeek(array $week)
    {
        for ($i = 0; $i < 7; $i++) {
            $days[$i] = date("Y-m-d", strtotime("{$week["Starts"] } + {$i} day"));
            $stmt = $this->db->prepare("INSERT INTO Days (Day, WeekId) VALUES (:day, :weekId)");
            $stmt->bindValue(":day", $days[$i]);
            $stmt->bindValue(":weekId", $week["WeekId"]);
            $stmt->execute();
        }
    }

    /**
     * Funkce z databáze vybere všechny dny, které jsou v daném týdnu a vrátí je jako asociativní pole objektů
     * @param int $weekId - dny kterého týdne
     * @return array|bool - pole objektů daných dnů v případě nalezení dnů v databázi nebo false při neúspěchu //todo asi
     */
    public function getDaysByWeekId(int $weekId): array|bool
    {
        $stmt = $this->db->prepare("SELECT * FROM Days WHERE WeekId = :weekId");
        $stmt->bindValue(":weekId", $weekId);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }


    public function getAllWeeks(): array|bool //TODO paging
    {
        $stmt = $this->db->query("SELECT * FROM Weeks ORDER BY Starts DESC ");
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * Funkce z databáze vybere poslední přidaný týden
     * @return array|bool pole s daty o hledaném týdnu, či false v případě neúspěchu
     */
    public function getLastInsertedWeek(): array|bool
    {
        $stmt = $this->db->query("SELECT max(Starts) as Starts, Ends, WeekId, WeekNumber, Status FROM Weeks");
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }

    /**
     * Funkce generuje týden a ukládá ho do databáze. Pokud v databázi žádný týden není, tak se dle data zavolání funkce vygeneruje týden následující.
     * Pokud databáze není prázdná, tak se na základě posledního přidaného týdně vygeneruje týden další a uloží do databáze.
     * @return array|bool //todo
     */
    public function generateNextWeek(): array|bool
    {
        $result = $this->getLastInsertedWeek();
        if ($result["Starts"] == null) {
            $data["Starts"] = date('Y-m-d', strtotime("next monday"));
            $data["Ends"] = date('Y-m-d', strtotime("next monday + 6 day"));
            $data["WeekNumber"] = date('W', strtotime("next monday"));
        } else {
            $data["Starts"] = date('Y-m-d', strtotime("{$result["Starts"]} next monday"));
            $data["Ends"] = date('Y-m-d', strtotime("{$result["Ends"]} + 7 day"));
            $data["WeekNumber"] = date('W', strtotime("{$result["Starts"]} next monday"));

        }

        $stmt = $this->db->prepare("INSERT INTO Weeks (WeekNumber, Starts, Ends) VALUES (:weekNumber, :starts, :ends)");
        $stmt->bindValue(":weekNumber", $data["WeekNumber"]);
        $stmt->bindValue(":starts", $data["Starts"]);
        $stmt->bindValue(":ends", $data["Ends"]);
        $stmt->execute();

        $lastWeek = $this->getLastInsertedWeek();
        if (!is_array($lastWeek)) //todo
            return false;

        $this->generateDaysOfWeek($lastWeek);
        $lastWeek["Days"] = $this->getDaysByWeekId($lastWeek["WeekId"]);
        return $lastWeek;

    }

    /**
     * Funkce z databáze vybere záznam dle požadovaného ID
     * @param int $id
     * @return array|bool
     */
    public function getShiftById(int $id): array|bool
    {
        $stmt = $this->db->prepare("SELECT * FROM Shifts WHERE ShiftId = :shiftId");
        $stmt->bindValue(":shiftId", $id);
        $stmt->execute();
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }


    public function createReservationSlots(array $data, $shiftId): bool
    {
            foreach ($data["ReservationSlots"] as $slot) {
                $stmt = $this->db->prepare("INSERT INTO ReservationSlots (Beginning, ShiftId) VALUES (:beginning, :shiftId)");
                $stmt->bindValue(":beginning", $slot);
                $stmt->bindValue(":shiftId", $shiftId);
                $stmt->execute();
            }
            return true;
    }

    /**
     * Funkce vytvoří nový záznam směny dle dat z požadavku.
     * @param array $data pole dat o záznamu směny
     * @return array|bool kompletní záznam směny uložený do databáze nebo false v případě neúspěchu vytvoření záznamu
     */
    public function registerShift(array $data): array|bool //TODO CHECK INPUTS - DOCTOR CAN WORK ONLY IN ONE ROOM PER SHIFT/day
    {
        try {
            $this->db->beginTransaction();
            $stmt = $this->db->prepare("INSERT INTO Shifts (Room, DoctorId, AssistantId, Start, End, DayId) 
                                            VALUES (:room, :doctorId, :assistantId, :start, :end, :dayId)");
            $stmt->bindValue(":room", $data["Room"]);
            $stmt->bindValue(":doctorId", $data["DoctorId"]);
            $stmt->bindValue(":assistantId", $data["AssistantId"]);
            $stmt->bindValue(":start", $data["Start"]);
            $stmt->bindValue(":end", $data["End"]);
            $stmt->bindValue(":dayId", $data["DayId"]);
            $stmt->execute();
            $lastId = $this->db->lastInsertId("Shifts");

            $this->setDayStatus($data["DayId"], 1);


            if($data["IsReservable"] === true)
                $this->createReservationSlots($data, $lastId);

            $this->db->commit();
            return $this->getShiftById($lastId);

        } catch (\PDOException $e) {
            $this->db->rollBack();
            throw $e;
        }
    }

    public function getWeekById(int $id)
    {
        $stmt = $this->db->prepare("SELECT * FROM Weeks WHERE WeekId = :weekId");
        $stmt->bindValue(":weekId", $id);
        $stmt->execute();
        $data = $stmt->fetch(PDO::FETCH_ASSOC);
        $data["Days"] = $this->getDaysByWeekId($id);
        return $data;
    }


    public function getShiftsByDay(int $dayId): bool|array
    {
        $stmt = $this->db->prepare("SELECT * FROM Shifts WHERE DayId = :dayId");
        $stmt->bindValue(":dayId", $dayId);
        $stmt->execute();
        $data = $stmt->fetchall(PDO::FETCH_ASSOC);
        foreach ($data as $key => $value) { //TODO better?
            $data[$key]["Doctor"] = $this->getPersonNameById($value["DoctorId"]);
            $data[$key]["Assistant"] = $this->getPersonNameById($value["AssistantId"]);
        }

        return $data;
    }

    public function getDayById(int $id): bool|array
    {
        $stmt = $this->db->prepare("SELECT * FROM Days WHERE DayId = :dayId");
        $stmt->bindValue(":dayId", $id);
        $stmt->execute();
        $data = $stmt->fetch(PDO::FETCH_ASSOC);
        $data["Shifts"] = $this->getShiftsByDay($id);
        return $data;
    }

    private function getPersonNameById(int $id)
    {
        $stmt = $this->db->prepare("SELECT FirstName, LastName FROM Persons WHERE PersonId = :personId");
        $stmt->bindValue(":personId", $id);
        $stmt->execute();
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }

    private function setDayStatus(int $dayId, int $status) //todo bool - nuluje false v db
    {
        $stmt = $this->db->prepare("UPDATE Days SET Status = :status WHERE DayId = :dayId");
        $stmt->bindValue(":status", $status);
        $stmt->bindValue(":dayId", $dayId);
        $stmt->execute();
    }

    private function setWeekStatus(int $weekId, int $status)
    {
        $stmt = $this->db->prepare("UPDATE Weeks SET Status = :status WHERE WeekId = :weekId");
        $stmt->bindValue(":status", $status);
        $stmt->bindValue(":weekId", $weekId);
        $stmt->execute();
    }


    public function updateShift(array $data): bool
    {
        $stmt = $this->db->prepare("UPDATE Shifts 
                                            SET Room = :room, DoctorId = :doctorId, AssistantId = :assistantId, Start = :start, End = :end 
                                            WHERE ShiftId = :shiftId");
        $stmt->bindValue(":room", $data["Room"]);
        $stmt->bindValue(":doctorId", $data["DoctorId"]);
        $stmt->bindValue(":assistantId", $data["AssistantId"]);
        $stmt->bindValue(":start", $data["Start"]);
        $stmt->bindValue(":end", $data["End"]);
        $stmt->bindValue(":shiftId", $data["ShiftId"]);
        return $stmt->execute();
    }

    public function checkWeekStatus()
    {
        $stmt = $this->db->query("SELECT WeekId FROM Weeks");
        $WeekIds = $stmt->fetchAll(PDO::FETCH_ASSOC);

        foreach ($WeekIds as $key => $value) {
            $stmt = $this->db->prepare("SELECT DISTINCT * FROM Days WHERE WeekId = :weekId AND Status = true");
            $stmt->bindValue(":weekId", $value["WeekId"]);
            $stmt->execute();
            if (count($stmt->fetchAll(PDO::FETCH_ASSOC)) < 7) //not every day of this week has set shifts
                $this->setWeekStatus($value["WeekId"], 0);
            else
                $this->setWeekStatus($value["WeekId"], 1);
        }

    }

    public function checkDayStatus()
    {
        $stmt = $this->db->query("SELECT DayId FROM Days");
        $DayIds = $stmt->fetchAll(PDO::FETCH_ASSOC);

        foreach ($DayIds as $key => $value) {
            $stmt = $this->db->prepare("SELECT * FROM Shifts WHERE DayId = :dayId");
            $stmt->bindValue(":dayId", $value["DayId"]);
            $stmt->execute();
            if (empty($stmt->fetchAll())) //No shifts this day
                $this->setDayStatus($value["DayId"], 0);
            else
                $this->setDayStatus($value["DayId"], 1);
        }
    }


}