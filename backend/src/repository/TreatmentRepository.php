<?php

namespace App\repository;

use DI\Container;
use PDO;

class TreatmentRepository
{
    private PDO $db;

    public function __construct(Container $container)
    {
        $this->db = $container->get('db');
    }

    public function createTreatment(array $data, DrugRepository $drugRepository): array|bool
    {
        try {
            $this->db->beginTransaction();

            if (isset($data["DrugId"]))
                $data["DrugSpecificationId"] = $drugRepository->createDrugSpec($data);


            if (empty($data["AssistantId"])) $data["AssistantId"] = null;
            if (empty($data["DrugSpecificationId"])) $data["DrugSpecificationId"] = null;
            if (empty($data["NextExamination"])) $data["NextExamination"] = null;


            $stmt = $this->db->prepare("INSERT INTO Treatments (Price, BeginDate, Status, Diagnosis, EmployeeId, AssistantId, PetId, DrugSpecificationId, NextExamination) VALUES (:price, :begin, :status, :diagnosis, :employeeId, :assistantId, :petId, :drugSpecId, :nextExam)");
            $stmt->bindValue(":price", $data["Price"]);
            $stmt->bindValue(":begin", date("Y-m-d"));
            $stmt->bindValue(":status", $data["Status"]);
            $stmt->bindValue(":diagnosis", $data["Diagnosis"]);
            $stmt->bindValue(":employeeId", $data["EmployeeId"]);
            $stmt->bindValue(":assistantId", $data["AssistantId"]);
            $stmt->bindValue(":petId", $data["PetId"]);
            $stmt->bindValue(":drugSpecId", $data["DrugSpecificationId"]);
            $stmt->bindValue(":nextExam", $data["NextExamination"]);
            if ($stmt->execute()) {
                $data["TreatmentId"] = $this->db->lastInsertId();
                $this->db->commit();
                return $data;
            }
        } catch (\TypeError $e) {
            $this->db->rollBack();
            throw $e;
        }
        return false;
    }

    public function getTreatmentsByPet(int $id): array|bool
    {
        $stmt = $this->db->prepare("SELECT * FROM Treatments WHERE PetId = :id ORDER BY Status DESC");
        $stmt->bindValue(":id", $id);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getTreatment(int $id): array|bool
    {
        $stmt = $this->db->prepare("SELECT * FROM Treatments WHERE TreatmentId = :id");
        $stmt->bindValue(":id", $id);
        $stmt->execute();
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }

    public function updateTreatment(array $data, DrugRepository $drugRepository): bool
    {
        try {
            $this->db->beginTransaction();
            /*

            if (isset($data["DrugSpecificationId"])) {
                if (isset($data["DrugId"]))
                    $drugRepository->updateDrugSpec($data);
                else {
                    $drugRepository->deleteDrugSpec($data["DrugSpecificationId"]);
                    $data["DrugSpecificationId"] = null;
                }
            }

            */

            switch ($data["flag"]) {
                case "delete":
                    $drugRepository->deleteDrugSpec($data["DrugSpecificationId"]);
                    $data["DrugSpecificationId"] = null;
                    break;
                case "update":
                    $drugRepository->updateDrugSpec($data);
                    break;
                case "create":
                    $data["DrugSpecificationId"] = $drugRepository->createDrugSpec($data);
                    break;

            }

            $stmt = $this->db->prepare("UPDATE Treatments SET Price = :price, Status = :status, Diagnosis = :diagnosis, NextExamination = :nextExam, DrugSpecificationId = :drugSpecId WHERE TreatmentId = :id");
            $stmt->bindValue(":price", $data["Price"]);
            $stmt->bindValue(":status", $data["Status"]);
            $stmt->bindValue(":diagnosis", $data["Diagnosis"]);
            $stmt->bindValue(":nextExam", $data["NextExamination"]);
            $stmt->bindValue(":drugSpecId", $data["DrugSpecificationId"]);
            $stmt->bindValue(":id", $data["TreatmentId"]);
            $result = $stmt->execute();
            $this->db->commit();
            return $result;

        } catch (\PDOException $e) {
            $this->db->rollBack();
            throw $e;
        }
    }

    public function getAllTreatments(): array
    {
        $stmt = $this->db->query("SELECT * FROM Treatments");
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }


}