<?php

namespace App\repository;

use DI\Container;
use PDO;

class DrugRepository
{
    private PDO $db;

    public function __construct(Container $container)
    {
        $this->db = $container->get('db');
    }

    public function createDrug(array $data): array|bool
    {
        $stmt = $this->db->prepare("INSERT INTO Drugs (Application, Contradiction, Name, Type, ActiveSubstance) VALUES (:application, :contradiction, :name, :type, :activeSubstance)");
        $stmt->bindValue(":application", $data["Application"]);
        $stmt->bindValue(":contradiction", $data["Contradiction"]);
        $stmt->bindValue(":name", $data["Name"]);
        $stmt->bindValue(":type", $data["Type"]);
        $stmt->bindValue(":activeSubstance", $data["ActiveSubstance"]);
        $stmt->execute();
        $data["DrugId"] = $this->db->lastInsertId();
        return $data;
    }

    public function getAllDrugs(): array|bool
    {
        $stmt = $this->db->query("SELECT * FROM Drugs");
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getDrugById(int $id): array|bool
    {
        $stmt = $this->db->prepare("SELECT * FROM Drugs WHERE DrugId = :id");
        $stmt->bindValue(":id", $id);
        $stmt->execute();
        return $stmt->fetch();
    }

    public function updateDrug(array $data): bool
    {
        $stmt = $this->db->prepare("UPDATE Drugs SET Application = :application, Contradiction = :contradiction, Name = :name, Type = :type, ActiveSubstance = :activeSubstance WHERE DrugId = :drugId");
        $stmt->bindValue(":drugId", $data["DrugId"]);
        $stmt->bindValue(":application", $data["Application"]);
        $stmt->bindValue(":contradiction", $data["Contradiction"]);
        $stmt->bindValue(":name", $data["Name"]);
        $stmt->bindValue(":type", $data["Type"]);
        $stmt->bindValue(":activeSubstance", $data["ActiveSubstance"]);
        return $stmt->execute();
    }

    public function createDrugSpec(array $data): int
    {
        $stmt = $this->db->prepare("INSERT INTO DrugSpecifications (Dosage, TimeOfUse, DrugId, EmployeeId) VALUES (:dosage, :timeOfUse, :drugId, :employeeId)");
        $stmt->bindValue(":dosage", $data["Dosage"]);
        $stmt->bindValue(":timeOfUse", $data["TimeOfUse"]);
        $stmt->bindValue(":drugId", $data["DrugId"]);
        $stmt->bindValue(":employeeId", $data["EmployeeId"]);
        $stmt->execute();
        return $this->db->lastInsertId();
    }

    public function getDrugSpecById(int $id): array
    {
        $stmt = $this->db->prepare("SELECT * FROM DrugSpecifications WHERE DrugSpecificationId = :id");
        $stmt->bindValue(":id", $id);
        $stmt->execute();
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }

    public function updateDrugSpec(array $data): array|bool
    {
        $stmt = $this->db->prepare("UPDATE DrugSpecifications SET Dosage = :dosage, TimeOfUse = :timeOfUse, DrugId = :drugId WHERE DrugSpecificationId = :id");
        $stmt->bindValue(":dosage", $data["Dosage"]);
        $stmt->bindValue(":timeOfUse", $data["TimeOfUse"]);
        $stmt->bindValue(":id", $data["DrugSpecificationId"]);
        $stmt->bindValue(":drugId", $data["DrugId"]);
        $stmt->execute();
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }

    public function deleteDrugSpec($id): bool
    {
        $stmt = $this->db->prepare("DELETE FROM DrugSpecifications WHERE DrugSpecificationId = :id");
        $stmt->bindValue(":id", $id);
        return $stmt->execute();
    }

}