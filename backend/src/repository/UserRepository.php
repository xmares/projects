<?php

namespace App\Repository;

use DI\Container;
use Firebase\JWT\JWT;
use PDO;


class UserRepository
{
    private PDO $db;

    public function __construct(Container $container)
    {
        $this->db = $container->get('db');
    }

    public function getUsers(int $levelOfAuth): array
    {
        if ($levelOfAuth > 30) //admin
            $stmt = $this->db->prepare("SELECT * FROM Persons WHERE Active = true");
        else $stmt = $this->db->prepare("SELECT * FROM Persons WHERE Active = true AND disc = 'Customers'");
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getUser(int $id, int $levelOfAuth): bool|array
    {
        if($levelOfAuth > 30) //can read access all profiles
            $stmt = $this->db->prepare("SELECT * FROM Persons JOIN Addresses USING (AddressId) WHERE PersonId = :personId");
        else //read only customers
            $stmt = $this->db->prepare("SELECT * FROM Persons JOIN Addresses USING (AddressId) WHERE PersonId = :personId AND disc = 'Customers'");
        $stmt->bindValue(":personId", $id);
        $stmt->execute();
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }

    public function getEmployeeDetails(int $id, string $role): bool|array
    {
        if ($role == "Doctors")
            $stmt = $this->db->prepare("SELECT * FROM EmployeeDetails JOIN Doctors USING (EmployeeDetailsId) WHERE DoctorId = :id");
        elseif ($role == "Assistants")
            $stmt = $this->db->prepare("SELECT * FROM EmployeeDetails JOIN Assistants USING (EmployeeDetailsId) WHERE AssistantId = :id");
        elseif ($role == "Administrators")
            $stmt = $this->db->prepare("SELECT * FROM EmployeeDetails JOIN Administrators USING (EmployeeDetailsId) WHERE AdministratorId = :id");
        else return false;
        $stmt->bindValue(":id", $id);
        $stmt->execute();
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }


    public function verifyLogin(string $login, string $password): array|bool
    {
        $stmt = $this->db->prepare("SELECT * FROM Logins JOIN Persons USING(Email) WHERE Email = :email");
        $stmt->bindValue(":email", $login);
        $stmt->execute();
        $user = $stmt->fetch();

        if ($user !== false) {
            $hash = $user["Password"];
            if (password_verify($password, $hash))
                return $user;
        }
        return false;
    }

    public function createToken(int $userId, string $userRole): string
    {
        $tokenKey = $_ENV['TOKEN_KEY'];
        $tokenPayload = [
            "userId" => $userId,
            "userRole" => $userRole,
        ];
        return JWT::encode($tokenPayload, $tokenKey, "HS256");
    }

    public function getAddressId(array $data): int|bool
    {
        $stmt = $this->db->prepare("SELECT AddressId FROM Addresses WHERE City = :city AND Street = :street AND StreetNumber = :streetNumber AND PostCode = :postCode");
        $stmt->bindValue(":city", $data["City"]);
        $stmt->bindValue(":street", $data["Street"]);
        $stmt->bindValue(":streetNumber", $data["StreetNumber"]);
        $stmt->bindValue(":postCode", $data["PostCode"]);
        $stmt->execute();
        return $stmt->fetch(\PDO::FETCH_COLUMN);
    }

    public function createAddress($data): int|bool
    {
        $stmt = $this->db->prepare("INSERT INTO Addresses (City, Street, StreetNumber, PostCode) VALUES (:city, :street, :streetNumber, :postCode)");
        $stmt->bindValue(":city", $data["City"]);
        $stmt->bindValue(":street", $data["Street"]);
        $stmt->bindValue(":streetNumber", $data["StreetNumber"]);
        $stmt->bindValue(":postCode", $data["PostCode"]);
        if ($stmt->execute())
            return $this->db->lastInsertId();
        return false;
    }

    public function createPerson($data): int|bool
    {
        $stmt = $this->db->prepare("INSERT INTO Persons (FirstName, LastName, AddressId, PhoneNumber, Email, disc) VALUES(:firstName, :lastName,:addressId, :phoneNumber, :email, :disc)");
        $stmt->bindValue(":firstName", $data["FirstName"]);
        $stmt->bindValue(":lastName", $data["LastName"]);
        $stmt->bindValue(":addressId", $data["AddressId"]);
        $stmt->bindValue(":phoneNumber", $data["PhoneNumber"]);
        $stmt->bindValue(":email", $data["Email"]);
        $stmt->bindValue(":disc", $data["Role"]);
        if ($stmt->execute()) return $this->db->lastInsertId();
        return false;
    }

    public function createLogin($data): bool
    {
        $stmt = $this->db->prepare("INSERT INTO Logins VALUES (:email, :password)");
        $stmt->bindValue(':email', $data['Email']);
        $stmt->bindValue(':password', password_hash($data['Password'], PASSWORD_DEFAULT));
        return $stmt->execute();
    }

    public function createCustomers(array $data): bool
    {
        $stmt = $this->db->prepare("INSERT INTO Customers VALUES (:customerId, :registered)");
        $stmt->bindValue(":customerId", $data["PersonId"]);
        $stmt->bindValue(":registered", time());
        return $stmt->execute();
    }

    public function createEmployeeDetails(array $data): int|bool //TODO DB scheme - UNIQUE EmplDetails
    {
        $stmt = $this->db->prepare("INSERT INTO EmployeeDetails (Position, Wage, AccountNumber) VALUES (:position, :wage, :accountNumber)");
        $stmt->bindValue(":position", $data['Position']);
        $stmt->bindValue(":wage", $data['Wage']);
        $stmt->bindValue(":accountNumber", $data['AccountNumber']);
        if ($stmt->execute())
            return $this->db->lastInsertId();
        return false;
    }

    public function createAdministrators(array $data): bool
    {
        $stmt = $this->db->prepare("INSERT INTO Administrators VALUES (:adminId, :employeeDetailsId)");
        $stmt->bindValue(":adminId", $data["PersonId"]);
        $stmt->bindValue(":employeeDetailsId", $data["EmployeeDetailsId"]);
        return $stmt->execute();
    }

    public function createAssistants(array $data): bool
    {
        $stmt = $this->db->prepare("INSERT INTO Assistants VALUES (:assistantId, :employeeDetailsId)");
        $stmt->bindValue(":assistantId", $data["PersonId"]);
        $stmt->bindValue(":employeeDetailsId", $data["EmployeeDetailsId"]);
        return $stmt->execute();
    }

    public function createDoctors(array $data): bool
    {
        $stmt = $this->db->prepare("INSERT INTO Doctors VALUES (:doctorId, :employeeDetailsId)");
        $stmt->bindValue(":doctorId", $data["PersonId"]);
        $stmt->bindValue(":employeeDetailsId", $data["EmployeeDetailsId"]);
        return $stmt->execute();
    }


    public function createRegistration(array $data): bool|array
    {
        //TODO checkInputs
        $this->db->beginTransaction();
        try {
            $addressId = $this->getAddressId($data); //checks existing addresses
            if ($addressId == false)
                $data["AddressId"] = $this->createAddress($data);
            else $data["AddressId"] = $addressId;
            $data["PersonId"] = $this->createPerson($data);
            switch ($data["Role"]) {
                case "Customers":
                    $this->createCustomers($data);
                    break;
                case "Administrators":
                    $data["EmployeeDetailsId"] = $this->createEmployeeDetails($data);
                    $this->createAdministrators($data);
                    break;
                case "Assistants":
                    $data["EmployeeDetailsId"] = $this->createEmployeeDetails($data);
                    $this->createAssistants($data);
                    break;
                case "Doctors":
                    $data["EmployeeDetailsId"] = $this->createEmployeeDetails($data);
                    $this->createDoctors($data);
                    break;
                default:
                    throw new \TypeError("invalid role");
            }
            $this->createLogin($data);

        } catch (\PDOException $e) {
            $this->db->rollBack();
            throw $e;
        }
        $this->db->commit();
        return $data;
    }

    public function getUserIdsOnAddress(int $addressId): array|bool
    {
        $stmt = $this->db->prepare("SELECT PersonId FROM Persons WHERE AddressId = :addressId");
        $stmt->bindValue(":addressId", $addressId);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);

    }

    public function updateAddress(array $data): bool //TODO CHECK PERSONS ADDRESSES - IF UPDATING ADDRESS WITH 2+ PERSONS - CREATE NEW ONE
    {
        $stmt = $this->db->prepare("UPDATE Addresses SET City = :city, Street = :street, StreetNumber = :streetNumber, PostCode = :postCode WHERE AddressId = :addressId");
        $stmt->bindValue(":city", $data["City"]);
        $stmt->bindValue(":street", $data["Street"]);
        $stmt->bindValue(":streetNumber", $data["StreetNumber"]);
        $stmt->bindValue(":postCode", $data["PostCode"]);
        $stmt->bindValue(":addressId", $data["AddressId"]);
        return $stmt->execute();
    }

    public function setExistingAddress(array $data): bool
    {
        $stmt = $this->db->prepare("UPDATE Persons SET AddressId = :addressId WHERE PersonId = :personId");
        $stmt->bindValue(":addressId", $data["AddressId"]);
        $stmt->bindValue(":personId", $data["PersonId"]);
        return $stmt->execute();
    }

    public function deleteAddressById(int $id): bool
    {
        $stmt = $this->db->prepare("DELETE FROM Addresses WHERE AddressId = :addressId");
        $stmt->bindValue(":addressId", $id);
        return $stmt->execute();

    }

    public function getEmailByUserId(int $personId): string|bool
    {
        $stmt = $this->db->prepare("SELECT Email FROM Persons WHERE PersonId = :personId");
        $stmt->bindValue(":personId", $personId);
        $stmt->execute();
        return $stmt->fetch()["Email"];
    }

    public function updateEmail(string $email, int $personId): bool
    {
        $oldEmail = $this->getEmailByUserId($personId);
        $stmt = $this->db->prepare("UPDATE Logins SET Email = :email WHERE Email = :oldEmail");
        $stmt->bindValue(":email", $email);
        $stmt->bindValue(":oldEmail", $oldEmail);
        return $stmt->execute();

    }

    public function updatePersons(array $data): bool
    {
        $stmt = $this->db->prepare("UPDATE Persons SET FirstName = :firstName, LastName = :lastName, AddressId = :addressId, PhoneNumber = :phoneNumber, Email = :email WHERE PersonId = :personId");
        $stmt->bindValue(":firstName", $data["FirstName"]);
        $stmt->bindValue(":lastName", $data["LastName"]);
        $stmt->bindValue(":addressId", $data["AddressId"]);
        $stmt->bindValue(":phoneNumber", $data["PhoneNumber"]);
        $stmt->bindValue(":email", $data["Email"]);
        $stmt->bindValue(":personId", $data["PersonId"]);
        return $stmt->execute();
    }

    public function updateEmployeeDetails($data): bool
    {
        $stmt = $this->db->prepare("UPDATE EmployeeDetails SET Position = :position, Wage = :wage, AccountNumber = :accountNumber WHERE EmployeeDetailsId = :employeeDetailsId");
        $stmt->bindValue(":position", $data["Position"]);
        $stmt->bindValue(":wage", $data["Wage"]);
        $stmt->bindValue(":accountNumber", $data["AccountNumber"]);
        $stmt->bindValue(":employeeDetailsId", $data["EmployeeDetailsId"]);
        return $stmt->execute();
    }

    public function updateProfile(array $data): bool
    {
        $this->db->beginTransaction();
        try {
            $usersWithSameAddress = $this->getUserIdsOnAddress($data["AddressId"]);
            if ($usersWithSameAddress !== false and count($usersWithSameAddress) > 1)
                $data["AddressId"] = $this->createAddress($data);
            else {
                $existingAddresses = $this->getAddressId($data); //check existing addresses if matches do not create duplicate record
                if ($existingAddresses !== false and $existingAddresses !== $data["AddressId"]) {
                    $this->deleteAddressById($data["AddressId"]);
                    $data["AddressId"] = $existingAddresses;
                    $this->setExistingAddress($data);
                }
                $this->updateAddress($data);
            }
            $this->updateEmail($data["Email"], $data["PersonId"]);
            $this->updatePersons($data);
            if (!empty($data["EmployeeDetailsId"]))
                $this->updateEmployeeDetails($data);

        } catch (\TypeError $e) {
            $this->db->rollBack();
            return false;
        }
        $this->db->commit();
        return true;
    }

    public function deleteLogin(string $email): bool
    {
        $stmt = $this->db->prepare("DELETE FROM Logins WHERE Email = :email");
        $stmt->bindValue(":email", $email);
        return $stmt->execute();

    }

    public function deleteEmployeeDetails(int $personId, string $role): bool
    {
        $employeeDetails = $this->getEmployeeDetails($personId, $role);
        if ($employeeDetails == false) return false;
        $stmt = $this->db->prepare("DELETE FROM EmployeeDetails WHERE EmployeeDetailsId = :employeeDetailsId");
        $stmt->bindValue(":employeeDetailsId", $employeeDetails["EmployeeDetailsId"]);
        $response = $stmt->execute();
        if ($response == false) return false;
        if ($role == "Doctors")
            $stmt = $this->db->prepare("DELETE FROM Doctors WHERE EmployeeDetailsId = :employeeDetailsId");
        elseif ($role == "Administrators")
            $stmt = $this->db->prepare("DELETE FROM Administrators WHERE EmployeeDetailsId = :employeeDetailsId");
        elseif ($role = "Assistants")
            $stmt = $this->db->prepare("DELETE FROM Assistants WHERE EmployeeDetailsId = :employeeDetailsId");
        $stmt->bindValue(":employeeDetailsId", $employeeDetails["EmployeeDetailsId"]);
        return $stmt->execute();
    }

    public function deactivateProfile(int $id): bool
    {
        $stmt = $this->db->prepare("UPDATE Persons SET Active = false WHERE PersonId = :personId");
        $stmt->bindValue(":personId", $id);
        return $stmt->execute();
    }

    public function deleteProfile(int $id): bool
    {

        $this->db->beginTransaction();
        try {
            $user = $this->getUser($id, 100); //todo
            $this->deleteLogin($user["Email"]);
            if ($user["disc"] == "Administrators" or $user["disc"] == "Assistants" or $user["disc"] == "Doctors")
                $this->deleteEmployeeDetails($id, $user["disc"]);

            $this->deactivateProfile($id);

            $usersWithSameAddress = $this->getUserIdsOnAddress($user["AddressId"]); //if address is used by multiple users
            if ($usersWithSameAddress !== false and count($usersWithSameAddress) < 2) {
                $this->deleteAddressById($user["AddressId"]);
            }
        } catch (\PDOException $e) {
            $this->db->rollBack();
            throw $e;
        }
        $this->db->commit();
        return true;
    }

    public function getAssistants(): bool|array
    {
        $stmt = $this->db->query("SELECT FirstName, LastName, PersonId FROM Persons WHERE disc = 'Assistants'");
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getDoctors(): bool|array
    {
        $stmt = $this->db->query("SELECT FirstName, LastName, PersonId FROM Persons WHERE disc = 'Doctors'");
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }


}