<?php

namespace App\repository;

use DI\Container;
use PDO;

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

class NotificationRepository
{

    private PDO $db;

    public function __construct(Container $container)
    {
        $this->db = $container->get('db');
    }

    public function getUpcomingSessions(): array
    {
        $stmt = $this->db->prepare("SELECT Pets.Name, Logins.Email, Treatments.NextExamination, Treatments.Diagnosis FROM Treatments
                                            JOIN Pets ON Treatments.PetId = Pets.PetId
                                            JOIN Customers ON Pets.OwnerId = Customers.CustomerId
                                            JOIN Persons on Customers.CustomerId = Persons.PersonId
                                            Join Logins on Persons.Email = Logins.Email                                   
                                            WHERE NextExamination = :tomorrow OR NextExamination = :in7Days"); // = OR =
        $stmt->bindValue(":tomorrow", date('Y-m-d', strtotime("+ 1 day")));
        $stmt->bindValue(":in7Days", date('Y-m-d', strtotime("+ 7 days")));
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);

    }

    public function notify(): array|bool
    {
        $data = $this->getUpcomingSessions();

        foreach ($data as $instance){
            //send mail
            $this->testMail($instance);
        }


        return $data;
    }


    public function testMail($instance)
    {
        //header("Access-Control-Allow-Origin: *"); //CORS - gmail server - NEEDED IF verbose debug output is enabled
        //Create an instance; passing `true` enables exceptions
        $mail = new PHPMailer(true);

        //Server settings
        //$mail->SMTPDebug = SMTP::DEBUG_SERVER;                      //Enable verbose debug output !!!NEED CORS - HEADER
        $mail->isSMTP();                                            //Send using SMTP
        $mail->Host = 'smtp.gmail.com';                     //Set the SMTP server to send through
        $mail->SMTPAuth = true;                                   //Enable SMTP authentication
        $mail->Username = 'IS.demo.email@gmail.com';                     //SMTP username
        $mail->Password = 'pvzyntadrkhzfytd';                               //SMTP password
        $mail->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;            //Enable implicit TLS encryption
        $mail->Port = 465;                                    //TCP port to connect to; use 587 if you have set `SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS`

        //Recipients
        $mail->setFrom('IS.demo.email@gmail.com');
        //$mail->addAddress('joe@example.net', 'Joe User');     //Add a recipient
        $mail->addAddress("{$instance["Email"]}");               //Name is optional
        //$mail->addReplyTo('info@example.com', 'Information');
        $mail->addCC('IS.demo.email@gmail.com');
        //$mail->addBCC('bcc@example.com');


        //Content
        $mail->isHTML(true);                       //Set email format to HTML
        $mail->Subject = "{$instance["Name"]} {$instance["Diagnosis"]}";
        $mail->Body = "Scheduled event: {$instance["NextExamination"]} <br>Pet: {$instance["Name"]} <br>Reason: {$instance["Diagnosis"]}";
        //$mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

        $mail->send();
    }
}