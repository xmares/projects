<?php

namespace App\repository;

use DI\Container;
use PDO;

class PetRepository
{
    private PDO $db;

    public function __construct(Container $container)
    {
        $this->db = $container->get('db');
    }

    public function checkOwnerId(int $id): bool
    {
        $stmt = $this->db->prepare("SELECT * FROM Persons WHERE PersonId = :personId");
        $stmt->bindValue(":personId", $id);
        return $stmt->execute();
    }


    public function createPet(array $data): bool|array
    {
        if (!$this->checkOwnerId($data["OwnerId"])) return false;
        else {
            $stmt = $this->db->prepare("INSERT INTO Pets (Name, DateOfBirth, OwnerId) VALUES (:name, :birth, :ownerId)");
            $stmt->bindValue(":name", $data["Name"]);
            $stmt->bindValue(":birth", $data["DateOfBirth"]);
            $stmt->bindValue(":ownerId", $data["OwnerId"]);
            if ($stmt->execute())
                $data["PetId"] = $this->db->lastInsertId();
            else return false;
        }
        return $data;
    }

    public function getOwners(): bool|array
    {
        $stmt = $this->db->query("SELECT PersonId, Email FROM Persons WHERE disc = 'Customers'");
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getAllPets(): bool|array
    {
        $stmt = $this->db->query("SELECT * FROM Pets");
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getOwnedPets(int $id): bool|array
    {
        $stmt = $this->db->prepare("SELECT * FROM Pets WHERE OwnerId = :ownerId");
        $stmt->bindValue(":ownerId", $id);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getPet(int $id): bool|array
    {
        $stmt = $this->db->prepare("SELECT * FROM Pets WHERE PetId = :petId");
        $stmt->bindValue(":petId", $id);
        $stmt->execute();
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }

    public function deletePet(int $id): bool
    {//TODO authorization
        $stmt = $this->db->prepare("DELETE FROM Pets WHERE PetId = :petId");
        $stmt->bindValue(":petId", $id);
        return $stmt->execute();
    }

    public function updatePet(array $data): array|bool
    {
        $stmt = $this->db->prepare("UPDATE Pets SET Name = :name, DateOfBirth = :birth, OwnerId = :ownerId WHERE PetId = :petId");
        $stmt->bindValue(":name", $data["Name"]);
        $stmt->bindValue(":birth", $data["DateOfBirth"]);
        $stmt->bindValue(":ownerId", $data["OwnerId"]);
        $stmt->bindValue(":petId", $data["PetId"]);
        $stmt->execute();
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }
}