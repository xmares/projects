<?php

namespace App\controller;

use App\repository\NotificationRepository;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class NotificationController
{
    private NotificationRepository $repository;

    public function __construct(NotificationRepository $repository)
    {
        $this->repository = $repository;
    }

    public function notify(ResponseInterface $response, RequestInterface $request): ResponseInterface
    {
        try{
        $data = $this->repository->notify();
        $json = json_encode($data);
        $response->getBody()->write($json);
        return $response->withStatus(201, "ok");
        } catch (\Exception $e){
            return $response->withStatus(400, "Emails not sent");
        }
    }


}