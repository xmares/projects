<?php

namespace App\controller;

use App\repository\DrugRepository;
use App\repository\PetRepository;
use App\repository\TreatmentRepository;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class TreatmentController
{
    private TreatmentRepository $repository;
    private Authorization $authorization;
    private PetRepository $petRepository;
    private DrugRepository $drugRepository;


    public function __construct(TreatmentRepository $repository, Authorization $authorization, PetRepository $petRepository, DrugRepository $drugRepository)
    {
        $this->repository = $repository;
        $this->authorization = $authorization;
        $this->petRepository = $petRepository;
        $this->drugRepository = $drugRepository;
    }

    public function createTreatment(ResponseInterface $response, RequestInterface $request): ResponseInterface
    {
        if (!$this->authorization->authorizationLevel30($request))
            return $this->authorization->unauthorizedAccess($response);

        $data = json_decode($request->getBody(), true);
        try {
            $data = $this->repository->createTreatment($data, $this->drugRepository);
            $json = json_encode($data);
            $response->getBody()->write($json);
            return $response->withStatus(201, "Created");
        } catch (\PDOException $e) {
            return $response->withStatus(400, $e->getMessage());
        }
    }

    public function isCustomerAuthorized(int $levelOfAuthorization, int $id, RequestInterface $request): bool
    {
        if ($levelOfAuthorization === 10) {
            $pet = $this->petRepository->getPet($id);
            if ($pet["OwnerId"] != $request->getAttribute('token')["userId"]) //customer accessing treatment for not their owned pet
                return false;
        }
        return true;
    }

    public function getTreatment(ResponseInterface $response, RequestInterface $request, int $id): ResponseInterface
    {
        $levelOfAuthorization = $this->authorization->getLevelOfAuthorization($request);
        if ($levelOfAuthorization === null)
            return $this->authorization->unauthorizedAccess($response);

        try {
            $data = $this->repository->getTreatment($id);
            if ($levelOfAuthorization <= 10 && !$this->isCustomerAuthorized($levelOfAuthorization, $data["PetId"], $request))
                return $this->authorization->unauthorizedAccess($response);
            $json = json_encode($data);
            $response->getBody()->write($json);
            return $response->withStatus(200, "OK");

        } catch (\PDOException $e) {
            return $response->withStatus(400, $e->getMessage());
        }
    }


    public function getTreatmentsByPet(ResponseInterface $response, RequestInterface $request, int $id): ResponseInterface
    {
        $levelOfAuthorization = $this->authorization->getLevelOfAuthorization($request);
        if ($levelOfAuthorization === null)
            return $this->authorization->unauthorizedAccess($response);

        try {
            $data = $this->repository->getTreatmentsByPet($id);
            if ($levelOfAuthorization <= 10 && !$this->isCustomerAuthorized($levelOfAuthorization, $id, $request))
                return $this->authorization->unauthorizedAccess($response);
            $json = json_encode($data);
            $response->getBody()->write($json);
            return $response->withStatus(200, "ok");

        } catch (\PDOException $e) {
            return $response->withStatus(400, $e->getMessage());
        }
    }

    public function updateTreatment(ResponseInterface $response, RequestInterface $request, int $id): ResponseInterface
    {
        if (!$this->authorization->authorizationLevel30($request))
            return $this->authorization->unauthorizedAccess($response);

        $data = json_decode($request->getBody(), true);
        $data["TreatmentId"] = $id;
        try {
            if ($this->repository->updateTreatment($data, $this->drugRepository))
                return $response->withStatus(200, "Updated");
            return $response->withStatus(400, "Not updated");

        } catch (\PDOException $e) {
            return $response->withStatus(400, $e->getMessage());
        }

    }

    public function getAllTreatments(ResponseInterface $response, RequestInterface $request): ResponseInterface
    {
        if (!$this->authorization->authorizationLevel20($request))
            return $this->authorization->unauthorizedAccess($response);

        try {
            $data = $this->repository->getAllTreatments();
            $json = json_encode($data);
            $response->getBody()->write($json);
            return $response->withStatus(200, "OK");
        } catch (\PDOException $e) {
            return $response->withStatus(400, $e->getMessage());
        }
    }


}