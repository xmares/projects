<?php

namespace App\controller;

use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class Authorization
{

    public function getTokenUserRole(RequestInterface $request): string
    {
        $tokenPayload = $request->getAttribute('token');
        return (string)$tokenPayload['userRole'];

    }

    public function authorizationLevel10(RequestInterface $request): bool
    {
        $role = $this->getTokenUserRole($request);
        return $role === "Customers" || $role === "Assistants" || $role === "Doctors" || $role === "Administrators";
    }

    public function authorizationLevel20(RequestInterface $request): bool
    {
        $role = $this->getTokenUserRole($request);
        return $role === "Assistants" || $role === "Doctors" || $role === "Administrators";
    }

    public function authorizationLevel30(RequestInterface $request): bool
    {
        $role = $this->getTokenUserRole($request);
        return $role === "Doctors" || $role === "Administrators";
    }

    public function authorizationLevel40(RequestInterface $request): bool
    {
        $role = $this->getTokenUserRole($request);
        return $role === "Administrators";
    }

    public function unauthorizedAccess(ResponseInterface $response): ResponseInterface
    {
        return $response->withStatus(403);
    }

    public function getLevelOfAuthorization(RequestInterface $request): int|null
    {
        if ($this->authorizationLevel40($request)) return 40;
        elseif ($this->authorizationLevel30($request)) return 30;
        elseif ($this->authorizationLevel20($request)) return 20;
        elseif ($this->authorizationLevel10($request)) return 10;
        return null;
    }
}