<?php

namespace App\controller;

use App\repository\PetRepository;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class PetController
{
    private PetRepository $repository;
    private Authorization $authorization;

    public function __construct(PetRepository $repository, Authorization $authorization)
    {
        $this->repository = $repository;
        $this->authorization = $authorization;
    }

    public function createPet(ResponseInterface $response, RequestInterface $request): ResponseInterface
    {
        if (!$this->authorization->authorizationLevel20($request))
            $this->authorization->unauthorizedAccess($response);

        $data = json_decode($request->getBody(), true);
        try {
            $data = $this->repository->createPet($data);
            $json = json_encode($data);
            $response->getBody()->write($json);
            if ($data == false) return $response->withStatus(400, "Bad input");
            return $response->withStatus(201, "Created");
        } catch (\PDOException $exception) {
            return $response->withStatus(400, $exception->getMessage());
        }
    }

    public function getOwners(ResponseInterface $response): ResponseInterface
    {
        try {
            $data = $this->repository->getOwners();
            $json = json_encode($data);
            $response->getBody()->write($json);
            return $response->withStatus(200, "ok");
        } catch (\PDOException $e) {
            return $response->withStatus(400, $e->getMessage());
        }

    }

    public function getAllPets(ResponseInterface $response, RequestInterface $request): ResponseInterface
    {
        if (!$this->authorization->authorizationLevel20($request))
            $this->authorization->unauthorizedAccess($response);
        try {
            $data = $this->repository->getAllPets();
            $json = json_encode($data);
            $response->getBody()->write($json);
            return $response->withStatus(200, "ok");
        } catch (\PDOException $e) {
            return $response->withStatus(400, $e->getMessage());
        }
    }

    public function getOwnedPets(ResponseInterface $response, RequestInterface $request, int $id): ResponseInterface
    {
        if (!$this->authorization->authorizationLevel10($request))
            return $this->authorization->unauthorizedAccess($response);

        try {
            $data = $this->repository->getOwnedPets($id);
            $json = json_encode($data);
            $response->getBody()->write($json);
            return $response->withStatus(200, "ok");

        } catch (\PDOException $e) {
            return $response->withStatus(400, $e->getMessage());
        }
    }

    public function getPet(ResponseInterface $response, RequestInterface $request, int $id): ResponseInterface
    {
        $levelOfAuthorization = $this->authorization->getLevelOfAuthorization($request);
        if ($levelOfAuthorization === null)
            return $this->authorization->unauthorizedAccess($response);
        try {
            $data = $this->repository->getPet($id);
            if ($levelOfAuthorization == 10 && $data["OwnerId"] != $request->getAttribute('token')["userId"]) //customer accessing not their owned pet
                return $this->authorization->unauthorizedAccess($response);
            $json = json_encode($data);
            $response->getBody()->write($json);
            return $response->withStatus(200, "ok");
        } catch (\PDOException $e) {
            return $response->withStatus(400, $e->getMessage());
        }

    }

    public function deletePet(ResponseInterface $response, RequestInterface $request, int $id): ResponseInterface
    {
        if (!$this->authorization->authorizationLevel20($request))
            return $this->authorization->unauthorizedAccess($response);
        try {
            $this->repository->deletePet($id);
            return $response->withStatus(200, "Deleted");
        } catch (\PDOException $e) {
            return $response->withStatus(400, $e->getMessage());
        }
    }

    public function updatePet(ResponseInterface $response, RequestInterface $request): ResponseInterface
    {
        if (!$this->authorization->authorizationLevel20($request))
            return $this->authorization->unauthorizedAccess($response);

        $data = json_decode($request->getBody(), true);
        try {
            $data = $this->repository->updatePet($data);
            $json = json_encode($data);
            $response->getBody()->write($json);
            return $response->withStatus(200, "updated");
        } catch (\PDOException $e) {
            return $response->withStatus(400, $e->getMessage());
        }

    }
}