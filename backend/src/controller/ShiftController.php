<?php

namespace App\controller;

use App\repository\ShiftRepository;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class ShiftController
{
    private ShiftRepository $repository;
    private Authorization $authorization; //todo

    public function __construct(ShiftRepository $repository, Authorization $authorization)
    {
        $this->repository = $repository;
        $this->authorization = $authorization;
    }

    /**
     * @param ResponseInterface $response
     * @return ResponseInterface v případě úspěchu vrací data o týdnu a jeho dnech, neúspěch - chybová hláška z výjimky
     */
    public function generateNextWeek(ResponseInterface $response): ResponseInterface
    {
        try {
            $data = $this->repository->generateNextWeek();
            $json = json_encode($data);
            $response->getBody()->write($json);
            return $response->withStatus(201, "ok");
        } catch (\Exception $e) {
            return $response->withStatus(400, $e->getMessage());
        }
    }


    public function getAllWeeks(ResponseInterface $response): ResponseInterface
    {
        try {
            $this->repository->checkWeekStatus(); //Weeks/days statuses updates by shifts
            $this->repository->checkDayStatus(); //Weeks/days statuses updates by shifts
            $data = $this->repository->getAllWeeks();
            $json = json_encode($data);
            $response->getBody()->write($json);
            return $response->withStatus(201, "OK");
        } catch (\PDOException $e) {
            return $response->withStatus(400, $e->getMessage());
        }
    }

    public function getWeekById(ResponseInterface $response, int $id): ResponseInterface
    {
        try {
            $data = $this->repository->getWeekById($id);
            $json = json_encode($data);
            $response->getBody()->write($json);
            return $response->withStatus(201, "OK");
        } catch (\PDOException $e) {
            return $response->withStatus(401, $e->getMessage());
        }
    }

    public function getDayById(ResponseInterface $response, int $id): ResponseInterface
    {
        try {
            $data = $this->repository->getDayById($id);
            $json = json_encode($data);
            $response->getBody()->write($json);
            return $response->withStatus(201, "OK");
        } catch (\PDOException $e) {
            return $response->withStatus(401, $e->getMessage());
        }
    }

    /**
     * Funkce dekóduje Json data z požadavku a předá je příslušné funkci z repozitáře
     * @param ResponseInterface $response
     * @param RequestInterface $request
     * @return ResponseInterface
     */
    public function registerShift(ResponseInterface $response, RequestInterface $request): ResponseInterface
    {
        try {
            $data = json_decode($request->getBody(), true);
            $this->repository->registerShift($data);
            $json = json_encode($data);
            $response->getBody()->write($json);
            return $response->withStatus(201, "ok");
        } catch (\Exception $e) {
            return $response->withStatus(400, $e->getMessage());
        }
    }

    /**
     * Funkce z databáze vybere záznam směny dle požadovaného id
     * @param ResponseInterface $response
     * @param RequestInterface $request
     * @param int $id - identifikační číslo směny
     * @return ResponseInterface
     */
    public function getShift(ResponseInterface $response, int $id):ResponseInterface
    {
        try{
            $data = $this->repository->getShiftById($id);
            $json = json_encode($data);
            $response->getBody()->write($json);
            return $response->withStatus(201, "OK");
        } catch (\Exception $e){
            return $response->withStatus(400, $e->getMessage());
        }
    }

    public function updateShift(ResponseInterface $response, RequestInterface $request, int $id):ResponseInterface
    {
        try{
            $data = json_decode($request->getBody(), true);
            $data["ShiftId"] = $id;
            $this->repository->updateShift($data);
            $data = $this->repository->getShiftById($id);
            $json = json_encode($data);
            $response->getBody()->write($json);
            return $response->withStatus(200, "UPDATED");
        } catch (\Exception $e){
            return $response->withStatus(400, $e->getMessage());
        }

    }

}