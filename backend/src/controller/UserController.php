<?php

namespace App\Controller;

use App\Repository\UserRepository;
use DI\Definition\AutowireDefinition;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Slim\Psr7\Response;


class UserController
{
    private UserRepository $repository;
    private Authorization $authorization;

    public function __construct(UserRepository $repository, Authorization $authorization)
    {
        $this->repository = $repository;
        $this->authorization = $authorization;
    }

    public function getUsers(ResponseInterface $response, RequestInterface $request): ResponseInterface
    {
        if (!$this->authorization->authorizationLevel20($request))
            return $this->authorization->unauthorizedAccess($response);

        try {
            $users = $this->repository->getUsers($this->authorization->getLevelOfAuthorization($request));
            $json = json_encode($users);
            $response->getBody()->write($json);
            return $response->withStatus(200, "OK");

        } catch (\PDOException $e) {
            return $response->withStatus(400, $e->getMessage());
        }
    }

    public function getUserById(ResponseInterface $response, RequestInterface $request, int $id): ResponseInterface
    {
        $levelOfAuthorization = $this->authorization->getLevelOfAuthorization($request);
        if ($levelOfAuthorization === null) // not even lowest level
            return $this->authorization->unauthorizedAccess($response);
        else if ($levelOfAuthorization <= 10 && $request->getAttribute('token')["userId"] != $id) { //customer - can read only his profile
            return $this->authorization->unauthorizedAccess($response);
        } else if ($request->getAttribute('token')["userId"] != $id)
            $levelOfAuthorization = 100; //accessing own profile

        try {
            $user = $this->repository->getUser($id, $levelOfAuthorization);
            if ($user !== false) {
                unset($user['Password']);
                $json = json_encode($user);
                $response->getBody()->write($json);
                return $response->withStatus(200, "OK");
            } else {
                return $response->withStatus(400, "User not found");
            }
        } catch (\PDOException $e) {
            return $response->withStatus(400, $e->getMessage());
        } catch (\TypeError $e) {
            return $response->withStatus(400, "Bad input");
        }
    }

    public function login(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        $loginData = json_decode($request->getBody(), true);
        $login = trim($loginData['Login']);
        $password = trim($loginData['Password']);

        if (empty($login) or empty($password))
            return $response->withStatus(400, "Bad input");
        elseif ($user = $this->repository->verifyLogin($login, $password)) {
            //create token
            $token = $this->repository->createToken($user['PersonId'], $user['disc']);
            $responseData = [
                'token' => $token
            ];
            $json = json_encode($responseData);
            $response->getBody()->write($json);
            return $response->withStatus(200, "OK");
        }
        return $response->withStatus(403, "Unauthorized");
    }

    public function register(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        return $this->extracted($request, $response, null);
    }

    public function authRegister(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface //admin - create accounts
    {
        if ($this->authorization->authorizationLevel40($request))
            return $this->extracted($request, $response, true);
        return $this->authorization->unauthorizedAccess($response);
    }


    public function extracted(ServerRequestInterface $request, ResponseInterface $response, $auth): ResponseInterface
    {
        $data = json_decode($request->getBody(), true);
        if (empty($auth)) //if not logged as admin - create only customers accounts
            $data["Role"] = "Customers";
        try {
            $user = $this->repository->createRegistration($data);
            if ($user !== false) {
                unset($user["Password"]); //prevent leaking password via frontend
                $json = json_encode($user);
                $response->getBody()->write($json);
                return $response->withStatus(201, "Registered");
            } else return $response->withStatus(400, "registration failed");


        } catch (\PDOException $e) {
            return $response->withStatus(400, $e->getMessage());
        } catch (\TypeError $e) {
            return $response->withStatus(400, "Bad input");
        }
    }

    public function getEmployeeDetails(ResponseInterface $response, RequestInterface $request, int $id, string $role): ResponseInterface
    {
        if (!$this->authorization->authorizationLevel20($request))
            return $this->authorization->unauthorizedAccess($response);

        try {
            $data = $this->repository->getEmployeeDetails($id, $role);
            $json = json_encode($data);
            $response->getBody()->write($json);
            return $response->withStatus(200, "OK");

        } catch (\PDOException $e) {
            return $response->withStatus(400, $e->getMessage());
        }

    }

    public function updateProfile(ResponseInterface $response, RequestInterface $request): ResponseInterface
    {
        $data = json_decode($request->getBody(), true);

        $levelOfAuthorization = $this->authorization->getLevelOfAuthorization($request);
        if ($levelOfAuthorization === null) // not even lowest level
            return $this->authorization->unauthorizedAccess($response);
        else if ($levelOfAuthorization <= 10 && $data["PersonId"] != $request->getAttribute('token')['userId']) { //customer - can update only his profile
            return $this->authorization->unauthorizedAccess($response);
        }

        try {
            if ($this->repository->updateProfile($data))
                return $response->withStatus(201, "Profile updated");
            else
                return $response->withStatus(400, "Not updated");
        } catch (\PDOException $e) {
            return $response->withStatus(401, $e->getMessage());
        }
    }

    public function deleteProfile(ResponseInterface $response, RequestInterface $request, int $id): ResponseInterface
    {
        if ($this->authorization->authorizationLevel40($request) || $request->getAttribute('token')["userId"] == $id) {
            $this->repository->deleteProfile($id);
            return $response->withStatus(200, "deleted");
        } else return $this->authorization->unauthorizedAccess($response);
    }


    /**
     * Seznam doktorů pro výpis směn
     * @param ResponseInterface $response
     * @param RequestInterface $request
     * @return ResponseInterface
     */
    public function getDoctors(ResponseInterface $response, RequestInterface $request): ResponseInterface
    {
        if ($this->authorization->authorizationLevel20($request)) {
            try {
                $data = $this->repository->getDoctors();
                $json = json_encode($data);
                $response->getBody()->write($json);
                return $response->withStatus(201, "ok");
            } catch (\PDOException $e) {
                return $response->withStatus(400, $e->getMessage());
            }
        } else return $this->authorization->unauthorizedAccess($response);
    }

    /**
     * Seznam asistentů pro výpis směn
     * @param ResponseInterface $response
     * @param RequestInterface $request
     * @return ResponseInterface
     */

    public function getAssistants(ResponseInterface $response, RequestInterface $request): ResponseInterface
    {
        if ($this->authorization->authorizationLevel20($request)) {
            try {
                $data = $this->repository->getAssistants();
                $json = json_encode($data);
                $response->getBody()->write($json);
                return $response->withStatus(201, "ok");
            } catch (\PDOException $e) {
                return $response->withStatus(400, $e->getMessage());
            }
        } else return $this->authorization->unauthorizedAccess($response);
    }


//testing
    public function getAddressId(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        $data = json_decode($request->getBody(), true);
        $id = $this->repository->getAddressId($data);
        $json = json_encode($id);
        $response->getBody()->write($json);
        return $response->withStatus(200, "OK");
    }

}