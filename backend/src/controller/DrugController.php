<?php

namespace App\controller;

use App\repository\DrugRepository;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class DrugController
{
    private DrugRepository $repository;
    private Authorization $authorization;

    public function __construct(DrugRepository $repository, Authorization $authorization)
    {
        $this->repository = $repository;
        $this->authorization = $authorization;
    }

    public function createDrug(ResponseInterface $response, RequestInterface $request): ResponseInterface
    {
        if (!$this->authorization->authorizationLevel30($request))
            return $this->authorization->unauthorizedAccess($response);

        $data = json_decode($request->getBody(), true);
        try {
            $data = $this->repository->createDrug($data);
            $json = json_encode($data);
            $response->getBody()->write($json);
            return $response->withStatus(201, "Created");

        } catch (\PDOException $e) {
            return $response->withStatus(400, $e->getMessage());

        }
    }

    public function getAllDrugs(ResponseInterface $response, RequestInterface $request): ResponseInterface
    {
        if (!$this->authorization->authorizationLevel30($request))
            return $this->authorization->unauthorizedAccess($response);

        try {
            $data = $this->repository->getAllDrugs();
            $json = json_encode($data);
            $response->getBody()->write($json);
            return $response->withStatus(200, "OK");

        } catch (\PDOException $e) {
            return $response->withStatus(400, $e->getMessage());
        }
    }

    public function getDrugById(ResponseInterface $response, RequestInterface $request, int $id): ResponseInterface
    {
        if (!$this->authorization->authorizationLevel10($request))
            return $this->authorization->unauthorizedAccess($response);
        try {
            $data = $this->repository->getDrugById($id);
            $json = json_encode($data);
            $response->getBody()->write($json);
            return $response->withStatus(200, "OK");

        } catch (\PDOException $e) {
            return $response->withStatus(400, $e->getMessage());
        }
    }

    public function updateDrug(ResponseInterface $response, RequestInterface $request, int $id): ResponseInterface
    {
        if (!$this->authorization->authorizationLevel30($request))
            return $this->authorization->unauthorizedAccess($response);

        $data = json_decode($request->getBody(), true);
        $data["DrugId"] = $id;
        try {
            if ($this->repository->updateDrug($data))
                $data = $this->repository->getDrugById($id);
            $json = json_encode($data);
            $response->getBody()->write($json);
            return $response->withStatus(200, "Updated");
        } catch (\PDOException $e) {
            return $response->withStatus(400, $e->getMessage());
        }
    }

    public function createDrugSpec(ResponseInterface $response, RequestInterface $request): ResponseInterface
    {
        if (!$this->authorization->authorizationLevel30($request))
            return $this->authorization->unauthorizedAccess($response);

        try {
            $data = json_decode($request->getBody(), true);
            $data["DrugSpecificationId"] = $this->repository->createDrugSpec($data);
            $json = json_encode($data);
            $response->getBody()->write($json);
            return $response->withStatus(201, "Created");

        } catch (\PDOException $e) {
            return $response->withStatus(400, $e->getMessage());
        }
    }

    public function getDrugSpecById(ResponseInterface $response, RequestInterface $request, int $id): ResponseInterface
    {
        if (!$this->authorization->authorizationLevel10($request))
            return $this->authorization->unauthorizedAccess($response);

        try {
            $data = $this->repository->getDrugSpecById($id);
            $json = json_encode($data);
            $response->getBody()->write($json);
            return $response->withStatus(200, "OK");

        } catch (\PDOException $e) {
            return $response->withStatus(400, $e->getMessage());
        }
    }

    public function updateDrugSpec(ResponseInterface $response, RequestInterface $request):ResponseInterface
    {
        if (!$this->authorization->authorizationLevel30($request))
            return $this->authorization->unauthorizedAccess($response);

        try{
            $data = json_decode($request->getBody(), true);
            $data = $this->repository->updateDrugSpec($data);
            $json = json_encode($data);
            $response->getBody()->write($json);
            return $response->withStatus(200, "Updated");

        } catch (\PDOException $e){
            return $response->withStatus(400, $e->getMessage());
        }
    }


    //todo delete, input check


}