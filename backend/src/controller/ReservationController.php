<?php

namespace App\controller;

use App\repository\ReservationRepository;
use App\repository\ShiftRepository;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class ReservationController
{
    private ReservationRepository $repository;
    private Authorization $authorization; //todo

    public function __construct(ReservationRepository $repository, Authorization $authorization)
    {
        $this->repository = $repository;
        $this->authorization = $authorization;
    }

    public function getReservationSlotsByShiftId(ResponseInterface $response, int $id):ResponseInterface
    {
        try {
            $data = $this->repository->getReservationSlotsByShiftId($id);
            $json = json_encode($data);
            $response->getBody()->write($json);
            return $response->withStatus(201, "OK");
        } catch (\PDOException $e) {
            return $response->withStatus(400, $e->getMessage());
        }
    }

    public function getReservationSlotsByDayId(ResponseInterface $response, int $id):ResponseInterface
    {
        try {
            $data = $this->repository->getReservationSlotsByDayId($id);
            $json = json_encode($data);
            $response->getBody()->write($json);
            return $response->withStatus(201, "OK");
        } catch (\PDOException $e) {
            return $response->withStatus(400, $e->getMessage());
        }

    }

    public function deleteReservationSlotById(ResponseInterface $response, int $id):ResponseInterface
    {
        try{
            if ($this->repository->deleteReservationSlotById($id))
                return $response->withStatus(201, "Deleted");
            else return $response->withStatus(400, "not deleted"); //todo
        } catch (\PDOException $e){
            return $response->withStatus(400, $e->getMessage());
        }
    }

    public function createReservation(ResponseInterface $response, RequestInterface $request):ResponseInterface
    {
        try{
            $data = json_decode($request->getBody(), true);
            $this->repository->createReservation($data);
            return $response->withStatus(201, "Ok");
        } catch (\PDOException $e){
            return $response->withStatus(400, $e->getMessage());
        }

    }

    //TODO procedure class
    public function getProcedures(ResponseInterface $response):ResponseInterface
    {
        try{
            $data = $this->repository->getProcedures();
            $json = json_encode($data);
            $response->getBody()->write($json);
            return $response->withStatus(201, "Ok");
        } catch (\PDOException $e){
            return $response->withStatus(400, $e->getMessage());
        }
    }

}