<?php

use App\controller\DrugController;
use App\Controller\HomeController;
use App\controller\NotificationController;
use App\controller\PetController;
use App\controller\ReservationController;
use App\controller\ShiftController;
use App\controller\TreatmentController;
use App\Controller\UserController;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Slim\App;
use Slim\Routing\RouteCollectorProxy;

//use Slim\Routing\RouteCollectorProxy;

return function (App $app) {

//Home
    $app->get('/', [HomeController::class, 'index']);
    $app->get('/notify', [NotificationController::class, 'notify']); //extra

//Users
    $app->post('/login', [UserController::class, 'login']);
    $app->post('/register', [UserController::class, 'register']);

    $app->group('/auth', function (RouteCollectorProxy $group) {
        $group->post('/register', [UserController::class, 'authRegister']);
        $group->get('/users', [UserController::class, 'getUsers']);
        $group->get('/user/{id}', [UserController::class, 'getUserById']);
        $group->get('/addressId', [UserController::class, 'getAddressId']);
        $group->get('/employeeDetails/{id}/{role}', [UserController::class, 'getEmployeeDetails']);
        $group->post('/updateProfile', [UserController::class, 'updateProfile']);
        $group->delete('/user/{id}', [UserController::class, 'deleteProfile']);

        $group->get('/owners', [PetController::class, 'getOwners']);
        $group->post('/pet', [PetController::class, 'createPet']);
        $group->get('/pets', [PetController::class, 'getAllPets']);
        $group->get('/pet/{id}', [PetController::class, 'getPet']);
        $group->delete('/pet/{id}', [PetController::class, 'deletePet']);
        $group->post('/updatePet', [PetController::class, 'updatePet']);
        $group->get('/pets/{id}', [PetController::class, 'getOwnedPets']);

        $group->post ('/treatment', [TreatmentController::class, 'createTreatment']);
        $group->post ('/treatment/{id}', [TreatmentController::class, 'updateTreatment']);
        $group->get ('/treatment/{id}', [TreatmentController::class, 'getTreatment']);
        $group->get ('/treatments/{id}', [TreatmentController::class, 'getTreatmentsByPet']);
        $group->get ('/treatments', [TreatmentController::class, 'getAllTreatments']);

        $group->post('/drug', [DrugController::class, 'createDrug']);
        $group->get('/drugs', [DrugController::class, 'getAllDrugs']);
        $group->get('/drug/{id}', [DrugController::class, 'getDrugById']);
        $group->post('/drug/{id}', [DrugController::class, 'updateDrug']);

        $group->post('/drugSpec/{id}', [DrugController::class, 'updateDrugSpec']);
        $group->get('/drugSpec/{id}', [DrugController::class, 'getDrugSpecById']);

        $group->get('/generateNextWeek', [ShiftController::class, 'generateNextWeek']); //todo - post?
        $group->get('/weeks', [ShiftController::class, 'getAllWeeks']);
        $group->get('/week/{id}', [ShiftController::class, 'getWeekById']);
        $group->get('/day/{id}', [ShiftController::class, 'getDayById']);


        $group->post('/shift', [ShiftController::class, 'registerShift']);
        $group->get('/shift/{id}', [ShiftController::class, 'getShift']);
        $group->post('/shift/{id}', [ShiftController::class, 'updateShift']);

        $group->get('/doctors', [UserController::class, 'getDoctors']);
        $group->get('/assistants', [UserController::class, 'getAssistants']);

        $group->get('/reservationSlotsByShift/{id}', [ReservationController::class, 'getReservationSlotsByShiftId']);
        $group->get('/reservationSlotsByDay/{id}', [ReservationController::class, 'getReservationSlotsByDayId']);
        $group->delete('/reservationSlots/{id}', [ReservationController::class, 'deleteReservationSlotById']);
        $group->get('/procedures', [ReservationController::class, 'getProcedures']);
        $group->post('/reservation', [ReservationController::class, 'createReservation']);




    });


    // CORS
    // - always respond successfully to options
    // - set up CORS headers - allow access from frontend on a different domain
    $app->options('/{routes:.+}', fn(ResponseInterface $response) => $response);
    $app->add(function (ServerRequestInterface $request, $handler) {
        $response = $handler->handle($request);
        return $response
            ->withHeader('Access-Control-Allow-Origin', '*')
            ->withHeader('Access-Control-Allow-Headers', 'Content-Type, Accept, Origin, Authorization')
            ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
    });

    // Not Found - handle all other routes as 404 Not Found error
    $app->map(['GET', 'POST', 'PUT', 'DELETE'], '/{routes:.+}', fn(ResponseInterface $response) => $response->withStatus(404, 'Not Found')
    );

};